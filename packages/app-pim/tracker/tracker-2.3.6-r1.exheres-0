# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2017 Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
# Distributed under the terms of the GNU General Purpose License v2

require gnome.org [ suffix=tar.xz ]
require bash-completion
require vala [ vala_dep=true ]
require gsettings test-dbus-daemon
require meson
require systemd-service

SUMMARY="A tool designed to extract information and metadata about your personal data"
HOMEPAGE="http://www.tracker-project.org/"

LICENCES="GPL-2 LGPL-2.1"
SLOT="2.0"
PLATFORMS="~amd64 ~x86"

MYOPTIONS="
    gtk-doc
    networkmanager [[ description = [ use NetworkManager to detect the current
        network status ] ]]
    systemd [[ description = [ install systemd user services ] ]]
"

DEPENDENCIES="
    build:
        dev-lang/perl:*[>=5.8.1]
        dev-util/intltool[>=0.40]
        gnome-desktop/gobject-introspection:1[>=0.9.5]
        virtual/pkg-config[>=0.20]
        gtk-doc? ( dev-doc/gtk-doc[>=1.8] )
    build+run:
        core/json-glib[>=1.0]
        dev-libs/glib:2[>=2.46.0]
        dev-db/sqlite:3[>=3.20.0-r1] [[ note = [ we only enabled FTS5 since 3.20.0-r1, which
            is required for tracker ] ]]
        dev-libs/appstream-glib
        dev-libs/icu:=[>=4.8.1.1] [[ note = [ upstream prefers libunistring, but we need
            ICU for charset detection anyway ] ]]
        dev-libs/libxml2:2.0[>2.6]
        gnome-desktop/libsoup:2.4[>2.40]
        media-libs/gstreamer:1.0
        media-libs/libmediaart:2.0[>=1.9.0][gobject-introspection?]
        media-libs/libpng:=[>=1.2]
        media-plugins/gst-plugins-base:1.0
        sys-apps/dbus[>1.3.1]
        sys-apps/util-linux [[ note = [ for libuuid ] ]]
        x11-libs/gdk-pixbuf:2.0[>=2.12.0]
        networkmanager? ( net-apps/NetworkManager )
        !app-pim/tracker:1.0 [[ description = [ Older ABI version, many file collissions ] ]]
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dfts=false
    -Dstemmer=disabled
    -Dunicode_support=icu
)

MESON_SRC_CONFIGURE_OPTIONS=(
    'bash-completion -Dbash_completions='"${BASHCOMPLETIONDIR}"
    'systemd -Dsystemd_user_services='"${SYSTEMDUSERUNITDIR}"
)

MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'networkmanager network_manager'
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gtk-doc docs'
)

MESON_SRC_CONFIGURE_TESTS=(
    '-Dfunctional_tests=true -Dfunctional_tests=false'
)

# Get stuck and fail after some time
RESTRICT="test"

src_test() {
    unset DISPLAY

    # redirect any configuration and user settings to temp via XDG variables
    export XDG_DATA_HOME=${TEMP}
    export XDG_CACHE_HOME=${TEMP}
    export XDG_CONFIG_HOME=${TEMP}

    # G_DBUS_COOKIE_SHA1_KEYRING_DIR requires 0700, ${TEMP} is 0755
    export G_DBUS_COOKIE_SHA1_KEYRING_DIR_IGNORE_PERMISSION=1
    export G_DBUS_COOKIE_SHA1_KEYRING_DIR=${TEMP}

    # use the memory backend for settings to ensure that the system settings in dconf remain
    # untouched by the tests
    export GSETTINGS_BACKEND=memory

    # the tracker-dbus/request test relies on g_debug() messages being output to stdout
    export G_MESSAGES_DEBUG=all

    test-dbus-daemon_start

    TZ=UTC LANG=C LC_ALL=C HOME="${TEMP}" test-dbus-daemon_src_test
    success=${?}

    test-dbus-daemon_stop
}

