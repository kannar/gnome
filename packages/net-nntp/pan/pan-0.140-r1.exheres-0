# Copyright 2009-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require option-renames [ renames=[ 'gnome-keyring keyring' 'gtk3 providers:gtk3' ] ]

SUMMARY="A newsreader for Gnome and real desktop environments"
DESCRIPTION="
Pan is a newsreader which attempts to be pleasing to both new and experienced users.
In addition to the standard newsreader features, Pan also supports yEnc, offline
newsreading, article filtering, multiple connections, and more features for power
users and alt.binaries fans.
"
HOMEPAGE="http://pan.rebelbase.com"
DOWNLOADS="${HOMEPAGE}/download/releases/${PV}/source/${PNV}.tar.bz2"

REMOTE_IDS="freecode:${PN}"

UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/download/releases/${PV}/"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    dbus
    keyring [[ description = [ Use gnome-keyring for password storage ] ]]
    notify [[ description = [ libnotify support for popup notifications (experimental) ] ]]
    spell
    ( providers: gtk2 gtk3 ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.40.6]
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        dev-libs/glib:2[>=2.26.0]
        dev-libs/gnutls[>=3.0.0]
        dev-libs/pcre[>=5.0]
        net-utils/gmime:2.6[>=2.5.5]
        dbus? ( sys-apps/dbus )
        keyring? ( gnome-desktop/gnome-keyring:1[>=3.2.0] )
        notify? ( x11-libs/libnotify[>=0.4.1] )
        spell? (
            app-spell/enchant:0[>=1.6.0]
            providers:gtk2? ( app-spell/gtkspell:2[>=2.0.16] )
            providers:gtk3? ( app-spell/gtkspell:3.0 )
        )
        providers:gtk2? ( x11-libs/gtk+:2[>=2.16.0] )
        providers:gtk3? ( x11-libs/gtk+:3 )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --with-gmime-crypto
    --with-gnutls
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    "keyring gkr"
    "notify libnotify"
)

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    dbus
    "spell gtkspell"
    'providers:gtk3 gtk3'
)

DEFAULT_SRC_COMPILE_PARAMS=(
    # Makefile:294: recipe for target 'libuu.a' failed
    AR=$(exhost --tool-prefix)ar
)

DEFAULT_SRC_INSTALL_EXCLUDE=( README.mingw README.windows README.windows.in )

